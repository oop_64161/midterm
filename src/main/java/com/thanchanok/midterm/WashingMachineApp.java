package com.thanchanok.midterm;

public class WashingMachineApp {
   public static void main(String[] args) {
       WashingMachine machine1 = new WashingMachine("WW80T4040CE/ST", "Samsung");
       WashingMachine machine2 = new WashingMachine("F2515STGV.AESPETH", "LG");
       WashingMachine machine3 = new WashingMachine("ES-W159T-SL", "Sharp");
       machine1.askMachineSetting();
       machine1.tellMachineStatus();
       machine1.wash();
       System.out.println();
       machine2.askMachineSetting();
       machine2.rinse();
       System.out.println();
       machine3.tellMachineStatus();
       machine3.wash();

   } 
}
