package com.thanchanok.midterm;

public class Clothes { // Attributes
    private String typeClothes;
    private String color;
    private char size;
    private int countStatus;

    public Clothes(String typeClothes, String color, char size) { // Constuctor
        this.typeClothes = typeClothes;
        this.color = color;
        this.size = size;
        this.countStatus = 0;
    }

    public void printStatusClothes() {
        if (countStatus == 1) {
            System.out.println("You wear this " + typeClothes + "( color: " + color + ", size: " + size + " )");
        } 
        if (countStatus == 0) {
            System.out.println("You not wear this " + typeClothes + "( color: " + color + ", size: " + size + " )");
        }
    }

    public void wearClothes() {
        if (countStatus == 0) {
            System.out.println("You wear " + typeClothes + "( color: " + color + ", size: " + size + " )");
            countStatus += 1;
        }else{
            System.out.println("You have already wear " + typeClothes + "( color: " + color + ", size: " + size + " )");
        }
    }

    public void undressClothes() {
        if (countStatus == 1) {
            System.out.println("You undress " + typeClothes + "( color: " + color + ", size: " + size + " )");
            countStatus -= 1;
        } else{
            System.out.println("You have already undress " + typeClothes + "( color: " + color + ", size: " + size + " )");
        }
    }
}
