package com.thanchanok.midterm;

import java.util.Scanner;

public class WashingMachine { // Attributes
    private String name;
    private String brand;
    private boolean water;
    private boolean detergent;

    public WashingMachine(String name, String brand) { // Constructor
        this.name = name;
        this.brand = brand;
    }
    
    public boolean askSettingWater() {
        int count_water = 0;
        while(count_water == 0){
            Scanner sc = new Scanner(System.in);
            System.out.println("Washing machine water setting: ");
            System.out.println("Press 1 Fill water");
            System.out.println("Press 2 Not Fill water");
            int ans = sc.nextInt();
            if (ans == 1) {
                System.out.println("Fill water process...");
                water = true;
                count_water += 1;
                break;
            } else if (ans == 2) {
                System.out.println("Not fill water process...");
                water = false;
                count_water += 1;
                break;
            } else {
                System.out.println("Error , plase press number again");
                continue;
            }
        }
        return water;
    }

    public boolean askSettingDetergent() {
        int count_detergent = 0;
        while(count_detergent == 0){
            Scanner sc = new Scanner(System.in);
            System.out.println("Washing machine detergent setting: ");
            System.out.println("Press 1 Add detergent");
            System.out.println("Press 2 Not add detergent");
            int ans = sc.nextInt();
            if (ans == 1) {
                System.out.println("Add detergent process...");
                detergent = true;
                count_detergent += 1;
                break;
            } else if (ans == 2) {
                System.out.println("Not add detergent process...");
                detergent = false;
                count_detergent += 1;
                break;
            } else {
                System.out.println("Error , plase press number again");
                continue;
            }
        }
        return detergent;
    }

    public void askMachineSetting(){
        askSettingWater();
        System.out.println();
        askSettingDetergent();
    }

    public void tellMachineStatus() {
        if (water == true && detergent == false) {
            System.out.println("This " + name + " washing machine have only water.");
        }
        else if (water == false && detergent == true) {
            System.out.println("This " + name + " washing machine have only detergent.");
        }
        else if (water == true && detergent == true) {
            System.out.println("This " + name + " washing machine have water and detergent.");
        }
        else if (water == false && detergent == false) {
            System.out.println("This " + name + " washing machine don't have water and detergent.");
        }
    }

    public void wash() {
        if (water == true && detergent == true) {
            System.out.println("This " + brand + " " + name + " washing machine is start process...");
        } else {
            System.out.println("This " + brand + " " + name + " washing machine can't process. Please check machine setting.");
        }
    }

    public void rinse() {
        if (water == true && detergent == false) {
            System.out.println("This " + brand + " " + name + "washing machine is start process...");
        } else {
            System.out.println("This " + brand + " " + name + "washing machine can't process. Please check machine setting.");
        }
    }
}
