package com.thanchanok.midterm;

public class ClothesApp {
   public static void main(String[] args) {
       Clothes clothes1 = new Clothes("T-shirt", "blue", 'M');
       Clothes clothes2 = new Clothes("trousers", "brown", 'S');
       Clothes clothes3 = new Clothes("skirt", "red", 'L');
       clothes1.wearClothes();
       clothes1.printStatusClothes();
       clothes1.undressClothes();
       clothes1.printStatusClothes();
       System.out.println();
       clothes2.wearClothes();
       clothes2.printStatusClothes();
       clothes2.undressClothes();
       clothes2.printStatusClothes();
       clothes2.undressClothes();
       System.out.println();
       clothes3.wearClothes();
       clothes3.wearClothes();
       clothes3.wearClothes();
   } 
}
